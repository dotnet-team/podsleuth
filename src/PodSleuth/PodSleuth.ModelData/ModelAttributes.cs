using System;

namespace PodSleuth.ModelData
{
    [Flags]
    public enum ModelAttributes
    {
        None        = 0,
        U2          = 1 << 0,
        HP          = 1 << 1,
        HarryPotter = 1 << 2,
        Pepsi       = 1 << 3,
        ProductRed  = 1 << 4
    }
}
