using System;

namespace PodSleuth.ModelData
{
    public enum ModelClass
    {
        Unknown,
        Grayscale,
        Color,
        Mini,
        Shuffle,
        Nano,
        Video,
        Slvr,
        Rokr,
        Classic,
        Touch,
        Phone
    }
}
