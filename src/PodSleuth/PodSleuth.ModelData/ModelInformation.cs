using System;
using System.Text;

namespace PodSleuth.ModelData
{
    public class ModelInformation
    {
        private const string icon_prefix = "multimedia-player-ipod";
    
        private ModelClass @class;
        private ModelColor color;
        private ModelAttributes attributes;
        private double generation;
        private string [] serial_codes;
        private double capacity;
        private string model_number;
        
        public ModelClass Class {
            get { return @class; }
            set { @class = value; }
        }
        
        public ModelColor Color {
            get { return color; }
            set { color = value; }
        }
        
        public ModelAttributes Attributes {
            get { return attributes; }
            set { attributes = value; }
        }
        
        public string [] SerialCodes {
            get { return serial_codes; }
            set { serial_codes = value; }
        }
        
        public double Capacity {
            get { return capacity; }
            set { capacity = value; }
        }
        
        public double Generation {
            get { return generation; }
            set { generation = value; }
        }
        
        public string ModelNumber {
            get { return model_number; }
            set { model_number = value; }
        }
        
        public string IconName {
            get { 
                return String.Format("{0}-{1}-{2}", icon_prefix, 
                    Class.ToString().ToLower(), Color.ToString().ToLower());
            }
        }
        
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}\t\t", Class);
            builder.AppendFormat("{0}\t", Generation);
            builder.AppendFormat("{0}\t", Color);
            builder.AppendFormat("{0}\t", Capacity);
            builder.AppendFormat("{0}\t", ModelNumber);
            builder.AppendFormat("{0}\t", Attributes);
            
            if(serial_codes == null || serial_codes.Length == 0) {
                builder.Append("Unknown");
            } else {
                for(int i = 0; i < serial_codes.Length; i++) {
                    builder.Append(serial_codes[i]);
                    if(i < serial_codes.Length - 1) {
                        builder.Append(",");
                    }
                }
            }
            
            return builder.ToString();
        }
    }
}
