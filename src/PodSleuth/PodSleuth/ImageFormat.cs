using System;

using PropertyList;

namespace PodSleuth
{
    public class ImageFormat
    {
        private ImageType type;
        private PixelFormat pixel_format;
        private int width;
        private int height;
        private int size;
        private int correlation_id;
        private int rotation;
        
        public ImageFormat(PlistDictionary format, string type) : this(format, ParseImageType(type))
        {
        }
        
        public ImageFormat(PlistDictionary format, ImageType type)
        {
            this.type = type;
            ParseFromPlist(format);
        }
        
        private void ParseFromPlist(PlistDictionary format)
        {
            correlation_id = ((PlistInteger)format["FormatId"]).Value;
            width = ((PlistInteger)format["RenderWidth"]).Value;
            height = ((PlistInteger)format["RenderHeight"]).Value;

            try {
                if (format.ContainsKey ("AlignRowBytes") && ((PlistBoolean)format["AlignRowBytes"]).Value && (width % 2) != 0) {
                    width = height = width + 1;
                }
            } catch {}
            
            if(format.ContainsKey("Rotation")) {
                rotation = ((PlistInteger)format["Rotation"]).Value;
            }
            
            if(format.ContainsKey("PixelFormat")) {
                pixel_format = ParsePixelFormat(((PlistString)format["PixelFormat"]).Value);
            }
        }
        
        private static PixelFormat ParsePixelFormat(string value)
        {    
            switch(value) {
                case "32767579": return PixelFormat.Iyuv;
                case "4C353635": return PixelFormat.Rgb565;
                case "42353635": return PixelFormat.Rgb565_BE;
                default:         return PixelFormat.Unknown;
            }
        }
        
        private static ImageType ParseImageType(string value)
        {
            switch(value) {
                case "ImageSpecifications": return ImageType.Photo;
                case "AlbumArt":            return ImageType.Album;
                case "ChapterImageSpecs":   return ImageType.Chapter;
                default:                    return ImageType.Unknown;
            }
        }
        
        public override string ToString()
        {
            return String.Format("corr_id={0},width={1},height={2},rotation={3},pixel_format={4},image_type={5}", 
                CorrelationId, Width, Height, Rotation, PixelFormat.ToString().ToLower(), Type.ToString().ToLower());
        }
        
        public ImageType Type {
            get { return type; }
        }
        
        public PixelFormat PixelFormat {
            get { return pixel_format; }
        }
        
        public int Width {
            get { return width; }
        }
        
        public int Height {
            get { return height; }
        }
        
        public int Size {
            get { return size; }
        }
        
        public int CorrelationId {
            get { return correlation_id; }
        }
        
        public int Rotation {
            get { return rotation; }
        }
    }
}
