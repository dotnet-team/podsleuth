using System;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

using Mono.Unix;
using Mono.Unix.Native;

namespace PodSleuth
{
    public static class ScsiReader
    {
        private const int IPOD_BUF_LENGTH = 252;
        private const int IPOD_SERIAL_PAGE = 0x80;
        private const int IPOD_PLIST_PAGE = 0xC0;
    
        public static string ReadSerial(string devicenode)
        {
            return ReadPage(devicenode, IPOD_SERIAL_PAGE).Trim().Substring(0, 11);
        }
        
        public static string ReadPlist(string devicenode)
        {
            return ReadPage(devicenode, IPOD_PLIST_PAGE);
        }
        
        private static string ReadPage(string devicenode, int page)
        {
            int fd = Syscall.open(devicenode, OpenFlags.O_RDWR);
            
            if(fd <= 0) {
                throw new UnixIOException(Stdlib.GetLastError());
            }
            
            try {
                int offset;
                byte [] result = Inquiry(fd, 0, 1, page, IPOD_BUF_LENGTH, 1, 0, out offset);
                if(page == IPOD_SERIAL_PAGE) {
                    return Encoding.ASCII.GetString(result, offset, result.Length - offset);
                } else {
                    StringBuilder builder = new StringBuilder();
                    for(int i = offset; i < result.Length; i++) {
                        byte [] page_buffer = Inquiry(fd, 0, 1, result[i], IPOD_BUF_LENGTH, 1, 0, out offset);
                        builder.Append(Encoding.ASCII.GetString(page_buffer, offset, page_buffer.Length - offset));
                    }
                    return builder.ToString();
                }
            } finally {
                Syscall.close(fd);
            }
        }
        
        [DllImport("libsgutils.dll")]
        private static extern int sg_ll_inquiry(int sg_fd, int cmddt, int evpd, int pg_op,
            IntPtr resp, int mx_resp_len, int noisy, int verbose);
            
        public static byte [] Inquiry(int sg_fd, int cmddt, int evpd, int pg_op, 
            int mx_resp_len, int noisy, int verbose, out int offset)
        {
            IntPtr buffer = Marshal.AllocHGlobal(mx_resp_len);
            offset = 4;
            
            try {
                if(sg_ll_inquiry(sg_fd, cmddt, evpd, pg_op, buffer, mx_resp_len, noisy, 0) != 0) {
                    throw new IOException(String.Format("Could not read SCSI codepage 0x{0:x}", pg_op));
                }
            
                byte length = Marshal.ReadByte(buffer, offset - 1);
                byte [] managed_buffer = new byte[length + offset];
                Marshal.Copy(buffer, managed_buffer, 0, length + offset);
                return managed_buffer;
            } finally {
                Marshal.FreeHGlobal(buffer);
            }
        }
    }
}
