using System;
using System.IO;
using System.Reflection;

namespace PodSleuth
{
    internal static class ResourceUtilities
    {
        public static StreamReader GetFileContents(string name)
        {
            Assembly asm = Assembly.GetCallingAssembly();
            Stream stream = asm.GetManifestResourceStream(name);
            return new StreamReader(stream); 
        }
    }
}
