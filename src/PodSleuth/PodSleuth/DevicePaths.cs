using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

using PodSleuth.ModelData;

namespace PodSleuth
{
    public class DevicePaths
    {
        //private ModelInformation model;
        private string root_directory;
        
        private string ipod_control_directory;
        private string device_directory;
        
        private string sysinfo_file;
        
        public DevicePaths(string mountPoint, ModelInformation modelHint)
        {
            //model = modelHint;
            root_directory = mountPoint;
            DiscoverPaths();
        }

        public override string ToString ()
        {
            StringBuilder builder = new StringBuilder ();
            builder.AppendFormat ("Root Directory:         {0}\n", root_directory);
            builder.AppendFormat ("iPod_Control Directory: {0}\n", ipod_control_directory);
            builder.AppendFormat ("Device Directory:       {0}\n", device_directory);
            builder.AppendFormat ("SysInfo File:           {0}\n", sysinfo_file);
            return builder.ToString ();
        }
        
        private void DiscoverPaths()
        {
            string path = Path.Combine(root_directory, "iPod_Control");
            if(Directory.Exists(path)) {
                ipod_control_directory = path;
            } else {
                return;
            }
            
            path = Path.Combine(ipod_control_directory, "Device");
            if(Directory.Exists(path)) {
                device_directory = path;
            } else {
                return;
            }
            
            path = Path.Combine(device_directory, "SysInfo");
            if(File.Exists(path)) {
                sysinfo_file = path;
            }
        }
        
        private string MakeRelative(string absolute)
        {
            if(absolute.StartsWith(root_directory)) {
                return absolute.Substring(root_directory.Length);
            }
            
            return absolute;
        }
        
        public string IpodControlDirectory {
            get { return MakeRelative(ipod_control_directory); }
        }
        
        public string DeviceDirectory {
            get { return MakeRelative(device_directory); }
        }
        
        public string SysInfoFile {
            get { return MakeRelative(sysinfo_file); }
        }
    }
}
