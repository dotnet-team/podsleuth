using System;

namespace PropertyList
{
    public class PlistString : PlistObject<string>
    {
        public PlistString(string value) : base(value)
        {
        }
        
        public override void Dump(int padding)
        {
            WriteLine(padding, "<string>{0}</string>", Value);
        }
    }
}
