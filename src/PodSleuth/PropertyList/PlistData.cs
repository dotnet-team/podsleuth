using System;

namespace PropertyList
{
    public class PlistData : PlistObject<byte []>
    {
        public PlistData(string value) : base(Convert.FromBase64String(value))
        {
        }
        
        public override void Dump(int padding)
        {
            WriteLine(padding, "<data>{0}</data>", Convert.ToBase64String(Value));
        }
    }
}
