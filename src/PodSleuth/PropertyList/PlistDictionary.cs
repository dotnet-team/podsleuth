using System;
using System.Collections;
using System.Collections.Generic;

namespace PropertyList
{
    public class PlistDictionary : PlistObjectBase, IEnumerable<KeyValuePair<string, PlistObjectBase>>
    {
        private List<string> keys;
        private Dictionary<string, PlistObjectBase> dict = new Dictionary<string, PlistObjectBase>();
        
        public PlistDictionary() : this(false)
        {
        }
        
        public PlistDictionary(bool keepOrder)
        {
            if(keepOrder) {
                keys = new List<string>();
            }
        }
        
        public PlistDictionary(Dictionary<string, PlistObjectBase> value) : this(value, false)
        {
        }
        
        public PlistDictionary(Dictionary<string, PlistObjectBase> value, bool keepOrder) : this(keepOrder)
        {
            foreach(KeyValuePair<string, PlistObjectBase> item in value) {
                Add(item.Key, item.Value);
            }
        }
        
        public PlistDictionary(IDictionary value) : this(value, false)
        {
        }
        
        public PlistDictionary(IDictionary value, bool keepOrder) : this(keepOrder)
        {
            foreach(DictionaryEntry item in value) {
                Add((string)item.Key, ObjectToPlistObject(item.Value));
            }
        }
        
        public override void Dump(int padding)
        {
            if(Count == 0) {
                WriteLine(padding, "<dict/>");
                return;
            }
            
            WriteLine(padding, "<dict>");
            
            foreach(KeyValuePair<string, PlistObjectBase> item in this) {
                Console.WriteLine("{0}<key>{1}</key>", String.Empty.PadRight(padding + 4), item.Key); 
                item.Value.Dump(padding + 4);
            }
            
            WriteLine(padding, "</dict>");
        }
        
        public void Clear()
        {
            if(keys != null) {
                keys.Clear();
            }
            
            dict.Clear();
        }
        
        public void Add(string key, PlistObjectBase value)
        {
            if(keys != null) {
                keys.Add(key);
            }
            
            if (dict.ContainsKey (key)) {
                Console.WriteLine ("Warning: ignoring duplicate key: {0} (null? {1} empty? {2})", key, key == null, key == "");
            } else {
                dict.Add(key, value);
            }
        }
        
        public bool Remove(string key)
        {
            if(keys != null) {
                keys.Remove(key);
            }
            
            return dict.Remove(key);
        }
        
        public bool ContainsKey(string key)
        {
            return dict.ContainsKey(key);
        }
        
        public PlistObjectBase this[string key] {
            get { return dict[key]; }
            set { dict[key] = value; }
        }
        
        public int Count {
            get { return dict.Count; }
        }

        private IEnumerator<KeyValuePair<string, PlistObjectBase>> GetEnumeratorFromKeys()
        {
            foreach(string key in keys) {
                yield return new KeyValuePair<string, PlistObjectBase>(key, dict[key]);
            }
        }

        public IEnumerator<KeyValuePair<string, PlistObjectBase>> GetEnumerator()
        {
            return keys == null ? dict.GetEnumerator() : GetEnumeratorFromKeys();
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
