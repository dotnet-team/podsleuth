using System;
using System.Xml;

namespace PropertyList
{
    public class PlistDocument : PlistObjectBase
    {
        private const double version = 1.0;
        
        private string raw_data;
        private PlistObjectBase root;
        
        public PlistDocument()
        {
        }
        
        public PlistDocument(PlistObjectBase root)
        {
            this.root = root;
        }
        
        public void LoadFromXmlFile(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            LoadFromXml(doc);
        }
        
        public void LoadFromXml(string data)
        {
            raw_data = data;
            
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(data);
            LoadFromXml(doc);
        }
        
        public void LoadFromXml(XmlDocument doc)
        {
            root = LoadFromNode(doc.DocumentElement.ChildNodes[0]);
        }
        
        private PlistObjectBase LoadFromNode(XmlNode node)
        {
            switch(node.Name) {
                case "dict":
                    return LoadFromDictionary(node);
                case "array":
                    // HACK: plist data in iPods is not even valid in some cases! Way to go Apple!
                    // This hack checks to see if they really meant for this array to be a dict.
                    if(node.ChildNodes[0].Name == "key") {
                        return LoadFromDictionary(node);
                    }
                    
                    return LoadFromArray(node);
                case "string":
                    return new PlistString(node.InnerText);
                case "integer":
                    return new PlistInteger(Convert.ToInt32(node.InnerText));
                case "real":
                    return new PlistReal(Convert.ToDouble(node.InnerText));
                case "false":
                    return new PlistBoolean(false);
                case "true":
                    return new PlistBoolean(true);
                case "data":
                    return new PlistData(node.InnerText);
            }
            
            throw new ApplicationException(String.Format("Plist Node `{0}' is not supported", node.Name));
        }
        
        private PlistDictionary LoadFromDictionary(XmlNode node)
        {
            XmlNodeList children = node.ChildNodes;
            if(children.Count % 2 != 0) {
                throw new DataMisalignedException("dict elements must have an even number of child nodes");
            }
            
            PlistDictionary dict = new PlistDictionary(true);
            
            for(int i = 0; i < children.Count; i += 2) {
                XmlNode keynode = children[i];
                XmlNode valnode = children[i + 1];
                
                if(keynode.Name != "key") {
                    throw new ApplicationException("expected a key node");
                }
                
                PlistObjectBase result = LoadFromNode(valnode);
                if(result != null) {
                    dict.Add(keynode.InnerText, result);
                }
            }
            
            return dict;
        }
        
        private PlistArray LoadFromArray(XmlNode node)
        {
            PlistArray array = new PlistArray();
            
            foreach(XmlNode child in node.ChildNodes) {
                PlistObjectBase result = LoadFromNode(child);
                if(result != null) {
                    array.Add(result);
                }
            }
            
            return array;
        }
        
        public PlistObjectBase Root {
            get { return root; }
            set { root = value; }
        }
        
        public string RawData {
            get { return raw_data; }
        }
        
        public override void Dump(int padding)
        {
            WriteLine(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            WriteLine(0, "<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" " +
                "\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
            WriteLine(0, "<plist version=\"{0}\">", version);
            root.Dump();
            WriteLine(0, "</plist>");
        }
    }
}
