using System;

namespace PropertyList
{
    public class PlistReal : PlistObject<double>
    {
        public PlistReal(double value) : base(value)
        {
        }
        
        public override void Dump(int padding)
        {
            WriteLine(padding, "<real>{0}</real>", Value);
        }
    }
}
