using System.Reflection;
using System.Runtime.CompilerServices;

[assembly:AssemblyVersion("0.6.7.*")]
[assembly:AssemblyTitle("PodSleuth")]
[assembly:AssemblyDescription("iPod device support library and tools")]
[assembly:AssemblyCopyright("Copyright (C) Novell, Inc.")]
[assembly:AssemblyCompany("Novell, Inc.")]

namespace PodSleuth
{
    internal static class InstallPaths
    {
        public static string CacheDirectory = "/var/cache/podsleuth";
    }
}

