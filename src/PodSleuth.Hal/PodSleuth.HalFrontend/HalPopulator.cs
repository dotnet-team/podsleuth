using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

using Hal;

using PodSleuth;

namespace PodSleuth.HalFrontend
{
    public static class HalPopulator
    {
        private const string HalNamespace = "org.podsleuth.";
        
        private static Hal.Device hal_device;
        private static DkDisk dk_disk;
        private static PodSleuth.Device pod_device;
        private static string mount_point;
        private static string fs_type;
        
        public static void Run(string [] args)
        {
            try {
                RunInternal(args);
            } catch(Exception e) {
                Console.Error.WriteLine("Pre-Mount Settings:");
                Console.Error.WriteLine("  - UDI:                    {0}", HalEnvironment.Udi);
                Console.Error.WriteLine("  - Block Device:           {0}", HalEnvironment.BlockDevice);
                Console.Error.WriteLine("  - Temporary Mount Point:  {0}", mount_point);
                Console.Error.WriteLine("  - FS Type:                {0}", fs_type); 
                
                if(pod_device != null && pod_device.PlistDocument != null && 
                    pod_device.PlistDocument.RawData != null) {
                    Console.Error.WriteLine("PList Dump:");
                    Console.Error.WriteLine();
                    Console.Error.WriteLine(pod_device.PlistDocument.RawData);
                }
                
                Console.Error.WriteLine();
                Console.Error.WriteLine("Exception:");
                Console.Error.WriteLine();
                Console.Error.WriteLine(e);
                
                Environment.Exit(1);
            }
        }
        
        private static void RunInternal(string [] args) 
        {
            if(HalEnvironment.BlockDevice == null || HalEnvironment.Udi == null) {
                throw new ApplicationException("This tool must be run by HAL");
            }
            
            hal_device = new Hal.Device(HalEnvironment.Udi);

            dk_disk = DkDisk.FindByDevice (HalEnvironment.BlockDevice ?? hal_device["block.device"]);
            
            bool private_mount = !(hal_device.GetPropertyBoolean("volume.is_mounted") || (dk_disk != null && dk_disk.IsMounted));
            
            if(private_mount) {
                mount_point = LowLevelMount.GenerateMountPoint();
                fs_type = hal_device["volume.fstype"];
                LowLevelMount.Mount(HalEnvironment.BlockDevice, mount_point, fs_type, true);
            } else {
                mount_point = dk_disk == null ? hal_device["volume.mount_point"] : dk_disk.MountPoint;
            }
            
            pod_device = new PodSleuth.Device(HalEnvironment.BlockDevice, mount_point);
            
            try {
                pod_device.Load();
            } catch(Exception e) {
                hal_device.SetPropertyBoolean(HalNamespace + "ipod.is_unknown", true);
                throw e;
            } finally {
                if(private_mount) {
                    LowLevelMount.Umount(mount_point);
                    Directory.Delete(mount_point);
                }
            }

            hal_device.SetPropertyInteger(HalNamespace + "version", 1);
            hal_device.SetPropertyBoolean(HalNamespace + "ipod.is_unknown", pod_device.IsUnknown);  
            
            if(pod_device.FirewireID != null) {
                hal_device.SetPropertyString(HalNamespace + "ipod.firewire_id", pod_device.FirewireID);
            }

            if(!pod_device.SerialNumber.Equals(SerialNumber.Zero) && 
                pod_device.SerialNumber.WholeSerialNumber != null) {
                hal_device.SetPropertyString(HalNamespace + "ipod.serial_number", 
                    pod_device.SerialNumber.WholeSerialNumber);
                    
                hal_device.SetPropertyString(HalNamespace + "ipod.production.factory_id",
                    pod_device.SerialNumber.ProductionFactoryID);
                    
                hal_device.SetPropertyInteger(HalNamespace + "ipod.production.year",
                    pod_device.SerialNumber.ProductionYear);
                    
                hal_device.SetPropertyInteger(HalNamespace + "ipod.production.week",
                    pod_device.SerialNumber.ProductionWeek);
                    
                hal_device.SetPropertyInteger(HalNamespace + "ipod.production.number",
                    pod_device.SerialNumber.ProductionNumber);
            }
            
            hal_device.SetPropertyBoolean(HalNamespace + "ipod.images.photos_supported", pod_device.PhotosSupported);
            hal_device.SetPropertyBoolean(HalNamespace + "ipod.images.album_art_supported", pod_device.AlbumArtSupported);
            hal_device.SetPropertyBoolean(HalNamespace + "ipod.images.sparse_album_art_supported", pod_device.SparseAlbumArtSupported);
            hal_device.SetPropertyBoolean(HalNamespace + "ipod.images.chapter_images_supported", pod_device.ChapterImagesSupported);
            
            if(hal_device.PropertyExists(HalNamespace + "ipod.images.formats")) {
                hal_device.RemoveProperty(HalNamespace + "ipod.images.formats");
            }
            
            if(pod_device.ImageFormats.Count > 0) {
                for(int i = 0; i < pod_device.ImageFormats.Count; i++) {
                    hal_device.StringListAppend(HalNamespace + "ipod.images.formats", 
                        pod_device.ImageFormats[i].ToString());
                }
            }
            
            if(!String.IsNullOrEmpty(pod_device.Paths.IpodControlDirectory)) {
                hal_device.SetPropertyString(HalNamespace + "ipod.control_path",
                    pod_device.Paths.IpodControlDirectory);
            }

            hal_device.SetPropertyString(HalNamespace + "ipod.firmware_version",
                pod_device.FirmwareVersion);
            
            if(hal_device.PropertyExists(HalNamespace + "ipod.capabilities")) {
                hal_device.RemoveProperty(HalNamespace + "ipod.capabilities");
            }
            
            foreach(FieldInfo field in typeof(DeviceCapabilities).GetFields()) {
                if(field.FieldType != typeof(DeviceCapabilities) || !field.IsLiteral) {
                    continue;
                }
                
                DeviceCapabilities cap = (DeviceCapabilities)field.GetValue(typeof(DeviceCapabilities));
                if((pod_device.Capabilities & cap) == cap && cap != DeviceCapabilities.None) {
                    hal_device.StringListAppend(HalNamespace + "ipod.capabilities", cap.ToString().ToLower());
                }
            }
            
            if(pod_device.IsUnknown) {
                hal_device.SetPropertyString(HalNamespace + "ipod.model.device_class", 
                    pod_device.IsShuffle ? "shuffle" : "unknown");
                return;
            }
            
            hal_device.SetPropertyString(HalNamespace + "ipod.model.device_class", 
                pod_device.ModelInformation.Class.ToString().ToLower());
                
            hal_device.SetPropertyString(HalNamespace + "ipod.model.shell_color", 
                pod_device.ModelInformation.Color.ToString().ToLower());
                
            hal_device.SetPropertyDouble(HalNamespace + "ipod.model.generation", 
                pod_device.ModelInformation.Generation);
                
            hal_device.SetPropertyString("info.icon_name", pod_device.ModelInformation.IconName);
            hal_device.Parent.SetPropertyString("info.icon_name", pod_device.ModelInformation.IconName);

            hal_device.SetPropertyString("info.desktop.icon", pod_device.ModelInformation.IconName);
            hal_device.Parent.SetPropertyString("info.desktop.icon", pod_device.ModelInformation.IconName);
        }
    }
}
