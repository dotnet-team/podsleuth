using System;

namespace PodSleuth.HalFrontend
{
    public static class HalEnvironment
    {
        private static string block_device;
        private static string udi;
        private static string hald_action;
        
        static HalEnvironment()
        {
            block_device = GetEnv("HAL_PROP_BLOCK_DEVICE");
            udi = GetEnv("UDI");
            hald_action = GetEnv("HALD_ACTION");
        }
        
        private static string GetEnv(string name)
        {
            string value = Environment.GetEnvironmentVariable(name);
            return value == null || value.Length == 0 ? null : value;
        }
        
        public static string BlockDevice {
            get { return block_device; }
        }
        
        public static string Udi {
            get { return udi; }
        }
        
        public static string HaldAction {
            get { return hald_action; }
        }
    }
}
