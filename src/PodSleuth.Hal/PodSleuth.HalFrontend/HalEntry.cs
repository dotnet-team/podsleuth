using System;

namespace PodSleuth.HalFrontend
{
    public static class HalEntry
    {
        public static void Main(string [] args)
        {            
            if(HalEnvironment.Udi != null && args[0] == "--discover") {
                HalPopulator.Run(args);
            } else if(HalEnvironment.Udi != null && args[0] == "--update") {
                try {
                    PodSleuth.ModelData.ModelTable.DownloadTableUpdate();
                } catch(Exception e) {
                    Console.Error.WriteLine("org.podsleuth.Update");
                    Console.Error.WriteLine(e.Message);
                    Environment.Exit(1);
                }
            } else {
                HalClient.Run(args);
            }
        }
    }
}
