using System;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;

using NDesk.DBus;
using Hal;

namespace PodSleuth.HalFrontend
{
    [Interface("org.podsleuth")]
    internal interface IPodSleuth
    {
        void Scan();
        void UpdateModelTable();
    }

    public static class HalClient
    {
        private const string HalNamespace = "org.podsleuth.";
        private static Version AsmVersion = Assembly.GetExecutingAssembly().GetName().Version;
        private static string Version = String.Format("{0}.{1}.{2}", 
            AsmVersion.Major, AsmVersion.Minor, AsmVersion.Build);
        
        private static Dictionary<string, string> arguments;
        private static bool have_updated = false;
        
        private static Dictionary<string, string> ParseCommandLineArguments(string [] args)
        {
            Dictionary<string, string> arguments = new Dictionary<string,string>();
            
            for(int i = 0; i < args.Length; i++) {
                string key = null;
                string value = null;
                
                if(args[i].StartsWith("--")) {
                    key = args[i].Substring(2);
                    value = key;
                    
                    int eq_offset = args[i].IndexOf('=');
                    
                    if(eq_offset > 3) {
                        key = args[i].Substring(2, eq_offset - 2);
                        value = args[i].Substring(eq_offset + 1);
                    } else if(i < args.Length - 1 && args[i + 1][0] != '-') {
                        value = args[i + 1];
                        i++;
                    }
                } else if(args[i][0] == '-' && args[i].Length == 2) {
                    key = args[i][1].ToString();
                    value = key;
                } else {
                    throw new ArgumentException("Invalid argument", args[i]);
                }
                
                if(value.StartsWith("\\-")) {
                    value = value.Substring(1);
                }
                
                if(arguments.ContainsKey(key)) {
                    arguments[key] = value;
                } else {
                    arguments.Add(key, value);
                }
            }
            
            return arguments;
        }
        
        public static void Run(string [] args)
        {
            arguments = ParseCommandLineArguments(args);
        
            if(arguments.ContainsKey("help")) {
                ShowHelp();
                return;
            } else if(arguments.ContainsKey("version")) {
                ShowVersion();
                return;
            }
        
            Hal.Manager manager = new Hal.Manager();
            int count = 0;
            
            foreach(Hal.Device ipod in manager.FindDeviceByStringMatchAsDevice("info.product", "iPod")) {
                foreach(Hal.Device volume in ipod.GetChildrenAsDevice(manager)) {
                    if(!volume.IsVolume || 
                        !IsVolumeMounted (volume) ||
                        !volume.PropertyExists("volume.fsusage") ||
                        volume.GetPropertyString("volume.fsusage") != "filesystem") {
                        continue;
                    }
                    
                    count++;
                    
                    if(arguments.ContainsKey("eject")) {
                        EjectIpod(volume.Volume);
                    } else {
                        if(!have_updated && arguments.ContainsKey("update")) {
                            Console.Write("Downloading PodSleuth table update... ");
                            
                            try {
                                volume.CastDevice<IPodSleuth>().UpdateModelTable();
                                Console.WriteLine("OK");
                            } catch(Exception e) {
                                Console.WriteLine("Error ({0})", e.Message);
                            }
                            
                            have_updated = true;
                        }
                        
                        if(arguments.ContainsKey("rescan")) {
                            Console.WriteLine("Rescanning device [{0}]", volume.Udi);
                            volume.CastDevice<IPodSleuth>().Scan();
                        }
                        
                        SleuthIpod(volume.Volume);
                    }
                }
            }
            
            if(count == 0) {
                Console.WriteLine("No iPods were found in the HAL device tree");
            }
        }
        
        private static void SleuthIpod(Hal.Volume volume)
        {
            if(!volume.PropertyExists(HalNamespace + "version")) {
                Console.WriteLine("Found an iPod device, but it is not known by PodSleuth:");
                Console.WriteLine("   Error: {0}* properties are missing", HalNamespace);
                Console.WriteLine("   UDI:          {0}", volume.Udi);
                Console.WriteLine("   Block Device: {0}", volume.GetPropertyString("block.device"));
                Console.WriteLine("   Mount Point:  {0}", MountPoint (volume));
                Console.WriteLine();
                Console.WriteLine("   Cause: PodSleuth may not be installed properly, the HAL daemon may need");
                Console.WriteLine("          to be restarted and/or the device needs to be refreshed.");
                Console.WriteLine();
                
                if(System.IO.File.Exists("/tmp/podsleuth-debug")) {
                    Console.WriteLine("   Note:  A PodSleuth debug log was found: /tmp/podsleuth-debug");
                    Console.WriteLine("          This file may provide more details or can be useful to");
                    Console.WriteLine("          developers. Please save it somewhere else and use it in");
                    Console.WriteLine("          a bug report. See http://banshee-project.org/PodSleuth");
                }
                
                return;
            }
            
            DumpSleuthableIpod(volume);
        }

        private static bool IsVolumeMounted(Hal.Device volume)
        {
            bool is_mounted = false;

            var dk_disk = DkDisk.FindByDevice (volume["block.device"] as string);
            if (dk_disk != null) {
                is_mounted = dk_disk.IsMounted;
            }

            if (!is_mounted && volume.PropertyExists("volume.is_mounted")) {
                is_mounted = volume.GetPropertyBoolean("volume.is_mounted");
            }

            return is_mounted;
        }

        private static string MountPoint (Hal.Device volume)
        {
            string mount_point = null;

            var dk_disk = DkDisk.FindByDevice (volume["block.device"] as string);
            if (dk_disk != null) {
                mount_point = dk_disk.MountPoint;
            }

            if (mount_point == null) {
                mount_point = volume["volume.mount_point"];
            }

            return mount_point;
        }
        
        private static bool IsVolumeReadOnly(Hal.Volume volume)
        {
            var dk_disk = DkDisk.FindByDevice (volume["block.device"] as string);
            if (dk_disk != null) {
                return dk_disk.IsReadOnly;
            }

            if(volume.PropertyExists("volume.is_mounted_read_only")) {
                return volume.GetPropertyBoolean("volume.is_mounted_read_only");
            }
            
            if(volume.PropertyExists("volume.fstype")) {
                if(volume["volume.fstype"] == "hfsplus") {
                    return true;
                }
            }
            
            return false;
        }
        
        private static string GetVolumeSizeString(Hal.Volume volume)
        {
            string format = "GiB";
            double value = volume.GetPropertyUInt64("volume.size") / 1000.0 / 1000.0 / 1000.0;
            
            if(value < 1.0) {
                format = "MiB";
                value *= 1000.0;
            }
            
            return String.Format("{0} {1}", (int)Math.Round(value), format); 
        }
        
        private static string GetCapabilities(Hal.Volume volume)
        {
            if(!volume.PropertyExists(HalNamespace + "ipod.capabilities")) {
                return "none";
            }
            
            string result = String.Empty;
            string [] caps = volume.GetPropertyStringList(HalNamespace + "ipod.capabilities");
            
            for(int i = 0; i < caps.Length; i++) {
                result += caps[i];
                if(i < caps.Length - 1) {
                    result += ", ";
                }
            }
            
            return result;
        }
        
        private static string GetProductionInfo(Hal.Volume volume)
        {
            try {
                string factory_id = volume[HalNamespace + "ipod.production.factory_id"];
                int year = volume.GetPropertyInteger(HalNamespace + "ipod.production.year");
                int week = volume.GetPropertyInteger(HalNamespace + "ipod.production.week");
                int number = volume.GetPropertyInteger(HalNamespace + "ipod.production.number");
                
                CultureInfo culture = CultureInfo.CurrentCulture;
                DateTime production_date = culture.Calendar.AddWeeks(new DateTime(year, 1, 1), week);
                
                return String.Format("{0} in {1}, {2} from factory {3}",
                    number, culture.DateTimeFormat.GetMonthName(production_date.Month), 
                    year, factory_id);
            } catch (Exception) {
                return "Unknown";
            }
        }
        
        private static void DumpSleuthableIpod(Hal.Volume volume)
        {
            Console.WriteLine("iPod Found [{0}]", volume.Udi);
            Console.WriteLine("  * Generic Device Properties");
            Console.WriteLine("    - Block Device:       {0}", volume["block.device"]);
            Console.WriteLine("    - Mount Point:        {0}", MountPoint (volume));
            Console.WriteLine("    - Read Only:          {0}", IsVolumeReadOnly(volume));
            Console.WriteLine("    - Volume Size:        {0}", GetVolumeSizeString(volume));
            Console.WriteLine("  * General iPod Properties");
            Console.WriteLine("    - Serial Number:      {0}", volume[HalNamespace + "ipod.serial_number"]);
			Console.WriteLine("    - Firewire ID:        {0}", volume[HalNamespace + "ipod.firewire_id"]);
            Console.WriteLine("    - Firmware Version:   {0}", volume[HalNamespace + "ipod.firmware_version"]);
            Console.WriteLine("    - iPod_Control:       {0}", volume[HalNamespace + "ipod.control_path"]);
            Console.WriteLine("    - Extra Capabilities: {0}", GetCapabilities(volume));
            Console.WriteLine("    - Production Info:    {0}", GetProductionInfo(volume));
            Console.WriteLine("  * iPod Model Properties");
            Console.WriteLine("    - Device Class:       {0}", volume[HalNamespace + "ipod.model.device_class"]);
                
            if(!volume.GetPropertyBoolean(HalNamespace + "ipod.is_unknown")) {
                if(volume.PropertyExists(HalNamespace + "ipod.model.generation")) {
                    Console.WriteLine("    - Generation:         {0}", volume.GetPropertyDouble(HalNamespace + "ipod.model.generation"));
                }

                if(volume.PropertyExists("ipod.model.shell_color")) {
                    Console.WriteLine("    - Shell Color:        {0}", volume[HalNamespace + "ipod.model.shell_color"]);
                }
            } else {
                Console.WriteLine("    - Model information could not be determined");
                Console.WriteLine("      Try a `podsleuth --update --rescan` or visit");
                Console.WriteLine("      http://banshee-project.org/IpodDataSubmit");
            }
            
            Console.WriteLine("  * Image Types Supported");
            Console.WriteLine("    - Photos:             {0}", volume.GetPropertyBoolean(HalNamespace + "ipod.images.photos_supported"));
            Console.WriteLine("    - Album Art:          {0}", volume.GetPropertyBoolean(HalNamespace + "ipod.images.album_art_supported"));
            Console.WriteLine("    - Sparse Album Art:   {0}", volume.GetPropertyBoolean(HalNamespace + "ipod.images.sparse_album_art_supported"));
            Console.WriteLine("    - Chapter Images:     {0}", volume.GetPropertyBoolean(HalNamespace + "ipod.images.chapter_images_supported"));
            
            if(arguments.ContainsKey("show-image-formats")) {
                string [] formats = volume.GetPropertyStringList(HalNamespace + "ipod.images.formats");
                if(formats != null && formats.Length > 0) {
                    Console.WriteLine("  * Image Types Supported");
                    foreach(string format in formats) {
                        Console.WriteLine("    - [{0}]", format);
                    }
                }
            }
            
            Console.WriteLine();
        }
        
        private static void EjectIpod(Hal.Volume volume)
        {
            Console.Write("Ejecting iPod [{0}]... ", volume.Udi);
            
            try {
                volume.Volume.Eject();
            } catch(Exception e) {
                Console.WriteLine("ERROR ({0})", e.Message);
                return;
            }
            
            Console.WriteLine("OK");
        }
        
        private static void ShowVersion()
        {
            Console.WriteLine("PodSleuth {0}, Copyright (C) 2007-2008 Novell, Inc.", Version);
            Console.WriteLine("http://banshee-project.org/PodSleuth");
        }
        
        private static void ShowHelp()
        {
            Console.WriteLine("Usage is: podsleuth [options]");
            
            Console.WriteLine();
            Console.WriteLine("Device Operations:");
            Console.WriteLine("  --rescan                Rescan all connected iPods");
            Console.WriteLine("  --update                Download an update model table");
            Console.WriteLine("  --eject                 Eject all connected iPods");
            Console.WriteLine("  --show-image-formats    Print detailed information for"); 
            Console.WriteLine("                          supported image formats");
            
            Console.WriteLine();
            Console.WriteLine("General Options:");
            Console.WriteLine("  --help                  Show this help");
            Console.WriteLine("  --version               Show version and copyright information");
            
            Console.WriteLine();
        }
    }
}
