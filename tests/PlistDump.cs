using System;
using System.IO;
using System.Collections.Generic;

using PodSleuth;
using PropertyList;

public static class PlistDumpTest
{
    public static void Main(string [] args)
    {
        string file = args[0];
        string plist_raw;
        bool dump_all = true;

        if(args.Length > 1) {
            switch(args[1]) {
                case "--image": dump_all = false; break;
                default: break;
            }
        }

        if(file.StartsWith("/dev/")) {
            plist_raw = ScsiReader.ReadPlist(file);
        } else {
            using(StreamReader reader = File.OpenText(file)) {
                plist_raw = reader.ReadToEnd();
            }
        }

        PlistDocument document = new PlistDocument();
        document.LoadFromXml(plist_raw);
        PlistDictionary root = document.Root as PlistDictionary;

        if(dump_all) {
            root.Dump();
            return;
        }

        foreach(string img_class in new string [] { 
            "ImageSpecifications", 
            "AlbumArt",
            "ChapterImageSpecs" }) {
            PlistDictionary formats = root[img_class] as PlistDictionary;
            foreach(KeyValuePair<string, PlistObjectBase> kvp in formats) {
                ImageFormat format = new ImageFormat(kvp.Value as PlistDictionary, img_class);
                Console.WriteLine(format);
            }
        }
    }
}

