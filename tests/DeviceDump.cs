using System;
using PodSleuth;

public static class DeviceDumpTest
{
    public static void Main(string [] args)
    {
        string device = args[0];
        string mount = args[1];

        Device ps_device = new Device (device, mount);
        ps_device.Load ();
        Console.WriteLine (ps_device);
    }
}

