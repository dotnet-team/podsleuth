using System;
using PodSleuth;

public static class PlistTest
{
    public static void Main(string [] args)
    {
        string device_node = args[0];
        string plist_raw;

        Console.WriteLine("Reading plist from SCSI code page...");
        plist_raw = ScsiReader.ReadPlist(device_node);
        Console.WriteLine(plist_raw);
    }
}

