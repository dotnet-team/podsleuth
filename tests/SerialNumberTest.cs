using System;
using PodSleuth;

public static class SerialNumberTest
{
    public static void Main(string [] args)
    {
        string device_node = args[0];
        string serial_raw;
        SerialNumber serial;

        if(device_node.StartsWith("/")) {
            Console.WriteLine("Reading serial number from SCSI code page...");
            serial_raw = ScsiReader.ReadSerial(device_node);
            Console.WriteLine("Got serial number [{0}]", serial_raw);
        } else {
            serial_raw = device_node;
        }

        Console.WriteLine("Parsing serial number...");
        serial = SerialNumber.Parse(serial_raw);
        Console.WriteLine(serial);
    }
}

