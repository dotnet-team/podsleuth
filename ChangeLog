commit e04812c0ad510ec8ce87d1b8bef36e2c78f531c8
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon May 10 13:56:55 2010

    Update NEWS for 0.6.7

commit 1e61d0ba3c17c87b02cf87709693d6a62b0321b9
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon May 10 13:51:51 2010

    Fix HAL activation on startup

commit 4667af8e9f5d0f3cb50e48aa42af0b3b8450edd0
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon May 10 12:19:35 2010

    Update NEWS

commit 685125fdf0d2e349b9848bcc29134f008bca88f4
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon May 10 12:13:25 2010

    Account for DeviceKit to UDisks rename
    
    Fall back to DeviceKit if UDisks doesn't exist - is runtime compatible
    with both setups.

commit 7b45117c2cd1a82923d5469ebd48631b9ec4b51a
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon May 10 12:12:55 2010

    Sync DkDisk with Banshee's

commit 2ea7775ae836b42667f12d7afadb90ae954a52c8
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon May 10 12:05:32 2010

    Start the HAL service if needed

commit e90a466000eba3f9ba78880b51cb66e603430994
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Sun Feb 21 14:48:55 2010

    [ipod-model-table] Add new 5th gen Nano model

commit c21bb19073ff98129bcb22ccb3257cff63d6728f
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Jan 26 16:55:51 2010

    Bump version now that 0.6.6 is released

commit 4a850c8726ec57772dd48a21dcf4b6e32474f8aa
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Jan 26 16:44:06 2010

    Update NEWS

commit 2842a815ee1e26a716d0e9a5c398774c5ec791d6
Author: Andy Clayton <clayt055@umn.edu>
Date:   Wed Jan 20 18:19:59 2010

    Add ipod.images.sparse_album_art_supported prop
    
    Signed-off-by: Gabriel Burt <gabriel.burt@gmail.com>

commit 49fc3d67a9ce098855204e961131a21946a95d01
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Dec 8 19:32:12 2009

    Bump version to 0.6.6

commit 7d0b3776985ffd6acc3f44a598c0fc27f56f8c08
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Dec 8 18:17:47 2009

    Install a dbus policy to fix podsleuth --rescan
    
    You will likely want to run autogen.sh with --sysconfdir=/etc

commit 8c1dda626d331b0b7e40ef21e7a828dfc195dd25
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Dec 8 18:09:15 2009

    Do the bare minimum to get working with DeviceKit
    
    Is not fully ported to DeviceKit; still relies on HAL extensively.
    But makes podsleuth work where DeviceKit is actually mounting the
    drive but HAL still is running.  The problem was podsleuth was reading
    HAL's volume.is_mounted property, which was false since DK had mounted
    it.

commit f6f56be9ffa17e9abd611de623cc2b653152272b
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 16:08:21 2009

    Updated NEWS, released 0.6.5

commit 0f5a3932184839b54e84420e4e0e90d9c886a0f5
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 16:08:15 2009

    [build] updated the upload-model-table target

commit 388e3b12648641d9950197c891dc27267d29e6b4
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 16:05:48 2009

    Ignore release-rc

commit 67693dc14784e688f010d4d40768e8efecf6a666
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 16:04:15 2009

    [build] release targets updated for git
    
    Apparently our last release was using subversion. It's been
    quite some time. Also updated what gets uploaded to the server,
    sha256sum it, keep the news, changes, etc.

commit df90cc78d807385f01de2f17dfbfc984b1113043
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 15:32:02 2009

    Added Black 5th gen Nano/16GB

commit b784d2cbfb7138daebd3d29205b93ba4a719c7d8
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 12:07:19 2009

    [build] generate ChangeLog from git log

commit 9775826cb8318f51b84e28cbef0ab8e12fcee4d4
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 12:01:34 2009

    [build] ensure aclocal runs with -I m4

commit 740d6eef75e0693708aeccc81965c71f3fd72544
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 11:58:24 2009

    Bump version to 0.6.5, add foreign automake option

commit 6c03e045cee252266b71760ddd88b996a07e50c0
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 11:58:10 2009

    Removed the old ChangeLog, everything in git log

commit ee5e73de0be4487cc75399796bfb29e12bf27b1f
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 11:53:40 2009

    Added .gitignore

commit 938b630a0c12d1150b14e528516c0e78dc1b4ef3
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 11:51:30 2009

    [build] Support both sgutils and sgutils2
    
    Expanded PODSLEUTH_CHECK_SGUTILS macro to look first for
    sgutils2 (sg_ll_inquiry), then the older sgutils. Export
    the .so name to LIBSGUTILS_SO.
    
    Change the dll name to libsgutils.dll, which now maps
    to the exported LIBSGUTILS_SO value.

commit 5526dfde2bd063eaa2ef47d2985afc9babfd27dc
Author: Aaron Bockover <abockover@novell.com>
Date:   Thu Oct 1 11:51:12 2009

    [build] link the PodSleuth.dll.config file to tests/

commit 53947c80cc22f06ff78eff529fa315eea951bdc5
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Jun 30 19:32:11 2009

    Respect AlignRowBytes image spec (BGO #554633)

commit 8c17bbb6e3fa9bd39c4c43c0e28a30a6795a189c
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Jun 30 18:02:11 2009

    2009-06-30  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* src/PodSleuth/PodSleuth/Device.cs: Patch from Nicolas Cortot to get some
    	3rd gen iPods working again (BGO #486661).  Modified by myself to be a
    	little more defensive/certain of not breaking currently-working models.

commit 52e4acfd2a46e6a8c5b37f4989c59f19af21232e
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue Jan 20 14:42:31 2009

    Released 0.6.4
    
    2009-01-20  Aaron Bockover  <abock@gnome.org>
    
        Released 0.6.4
    
        * configure.ac: Bumped version
    
        * NEWS: Updated
    
    
    svn path=/trunk/; revision=75

commit 2074464a4a0a982945b0bad651d8a585ef48609e
Author: Gabriel Burt <gburt@src.gnome.org>
Date:   Tue Jan 20 13:36:14 2009

    Update for release
    
    svn path=/trunk/; revision=74

commit 6a78e954e2dcbbf761f5bfc0260cb738fdb7520b
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Sat Jan 17 11:59:43 2009

    Patch from Ariel Hernandez fixing incorrect model number for silver 160GB
    
    2009-01-17  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* data/ipod-model-table: Patch from Ariel Hernandez fixing incorrect model
    	number for silver 160GB iPod classic (BGO #567762)
    
    
    svn path=/trunk/; revision=73

commit bb3319135dd1cc9f321d18a14366359f79bfeef9
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Tue Jan 13 09:10:14 2009

    Patch from Bastien Nocera setting the info.desktop.icon property (BGO
    
    2009-01-13  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Patch from
    	Bastien Nocera setting the info.desktop.icon property (BGO #518772)
    
    
    svn path=/trunk/; revision=72

commit b36ef9cba91d034427ec593192016c2358e57255
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Fri Dec 19 10:36:55 2008

    Update from libgpod, and extrapolate some SNs based on the new info.
    
    2008-12-19  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* data/ipod-model-table: Update from libgpod, and extrapolate some SNs
    	based on the new info.
    
    
    svn path=/trunk/; revision=71

commit 31990f20dab4448a72f42627997a1d032a352b50
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Fri Dec 19 10:18:55 2008

    Ignore duplicate keys in a dictionary (BNC #430663)
    
    2008-12-19  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* src/PodSleuth/PropertyList/PlistDictionary.cs: Ignore duplicate keys in
    	a dictionary (BNC #430663)
    
    
    svn path=/trunk/; revision=70

commit e96eb61ddc828c33dcc7d3544f9a0bc0255167ab
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Fri Dec 19 09:58:58 2008

    Fix crasher caused by production info not being where we expect it (BGO
    
    2008-12-19  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Fix crasher caused
    	by production info not being where we expect it (BGO #564658)
    
    
    svn path=/trunk/; revision=69

commit ce1b87f403f3eb924855cf338c0384d494167c2a
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Wed Oct 1 16:18:49 2008

    Add a lot of new info taken from libgpod.
    
    2008-10-01  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* data/ipod-model-table: Add a lot of new info taken from libgpod.
    
    
    svn path=/trunk/; revision=68

commit be7913d2460afd087069f867717e8afd788338b9
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Wed Oct 1 14:38:05 2008

    Fix model number for new Nanos. I wasn't aware that the "Model No." listed
    
    2008-10-01  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* data/ipod-model-table: Fix model number for new Nanos.  I wasn't aware
    	that the "Model No." listed on the back of the device is not what we want.
    	Instead, we want a substring of the part number listed next to the device
    	description ("iPod nano 8GB Black") on the box.
    
    
    svn path=/trunk/; revision=67

commit 9600fe70c9fb994338283278774dc3ba57cae5a8
Author: Gabriel Burt <gabriel.burt@gmail.com>
Date:   Mon Sep 29 10:55:56 2008

    Add support for new plist format that specifies support image formats in
    
    2008-09-29  Gabriel Burt  <gabriel.burt@gmail.com>
    
    	* src/PodSleuth/PodSleuth/Device.cs: Add support for new plist format that
    	specifies support image formats in an array instead of as entries in a
    	dictionary.  This fix means that 4th gen Nano devices and their
    	comtemporaries should properly detect whether they support album art
    	again.
    
    	* data/ipod-model-table: Fix duplicate serial number and missing comment
    	semicolons.
    
    
    svn path=/trunk/; revision=66

commit 7f8a09ba593789c3a072ed51e8097a5adfa51029
Author: Aaron Bockover <abock@gnome.org>
Date:   Mon Sep 29 10:32:30 2008

    Updated a lot of model information, hopefully it's all correct
    
    2008-09-29  Aaron Bockover  <abock@gnome.org>
    
        * data/ipod-model-table: Updated a lot of model information,
        hopefully it's all correct
    
    
    svn path=/trunk/; revision=65

commit 1017c785643e39c9c482c3930befb01259589436
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Thu Sep 11 16:31:21 2008

    Fixed targets
    
    svn path=/trunk/; revision=63

commit efa2900a01f0fd1dfd43ef9893810d1272da529e
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu Sep 11 16:29:10 2008

    Released 0.6.3
    
    2008-09-11  Aaron Bockover  <abock@gnome.org>
    
        Released 0.6.3
    
        * configure.ac: Bumped version
    
        * NEWS: Updated
    
    
    svn path=/trunk/; revision=62

commit 1ca28b1ae0f39cca58c8f41c0fbd456bed7eaa3a
Author: Gabriel Burt <gburt@novell.com>
Date:   Wed Sep 3 07:38:54 2008

    When a device is plugged in during bootup and the HAL callout is called,
    
    2008-09-02  Gabriel Burt  <gburt@novell.com>
    
    	* src/PodSleuth.Hal/Hal/Device.cs: When a device is plugged in during
    	bootup and the HAL callout is called, the HAL DBus service isn't yet
    	available.  Sleep in 1s increments up to 10s for the service to be
    	available before failing (BGO #488209).
    
    
    svn path=/trunk/; revision=61

commit 8f7a0ff9d973bf2579bb5387c91e72f3355d20fc
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu Jun 5 11:49:49 2008

    Released 0.6.2
    
    2008-06-05  Aaron Bockover  <abock@gnome.org>
    
        Released 0.6.2
    
        * NEWS: Updated
    
    
    svn path=/trunk/; revision=59

commit 4bc474848d1185c08c59af1436dbd6816c4d2cf7
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Jun 4 18:29:01 2008

    Bump to 0.6.2
    
    2008-06-04  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac: Bump to 0.6.2
    
        * src/PodSleuth.Hal/podsleuth.in: @expanded_libdir@ -> @prefix@/lib
    
    
    svn path=/trunk/; revision=58

commit 97f6f522cf314b922d90975fa967e12aa4165cd3
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Jun 4 18:08:41 2008

    Use @prefix@/lib as the exec path, not @expanded_libdir@, which is arch
    
    2008-06-04  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/hal-podsleuth.in: Use @prefix@/lib as the exec path,
        not @expanded_libdir@, which is arch specific (BGO #532386)
    
    
    svn path=/trunk/; revision=57

commit 1c757148d1fb3f22100f678045cd0dd502aa18ab
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Jun 4 17:15:58 2008

    Do not merge the iPod_Control path if it is unset; fixes a small bug where
    
    2008-06-04  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Do not merge
        the iPod_Control path if it is unset; fixes a small bug where if the
        directory contents are moved out of the way, the model detection fails
        to complete
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Add some property
        exists checks to be slightly more robust
    
        * tests/DeviceDump.cs: Added a small device dumper to test the non-HAL
        device stuff
    
        * src/PodSleuth/PodSleuth/Device.cs:
        * src/PodSleuth/PodSleuth/DevicePaths.cs: Added ToString override to
        dump useful information
    
    
    svn path=/trunk/; revision=56

commit f38196a354bf12ae4942e45dcfe3862c715c687c
Author: Aaron Bockover <abock@gnome.org>
Date:   Fri May 30 15:23:37 2008

    Apply patch to fix the /console client/ - note this /does not/ address the
    
    2008-05-30  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs:
        Apply patch to fix the /console client/ - note this /does not/
        address the issue that many are reporting about iPods not showing
        up; from what I can tell that's a /distro level issue/ where HAL
        is not executing the callout from the FDI file we install
    
    
    svn path=/trunk/; revision=55

commit b223b4c3604a5454f90be5eedf8890fce92e088f
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Wed Dec 19 15:40:57 2007

    Emergency build release, 0.6.1
    
    svn path=/trunk/; revision=53

commit 22a482410c81b20f73f643763d7200f16c504b38
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Dec 19 15:33:43 2007

    Only use expanded_libdir for the configure warning; use the unexpanded
    
    2007-12-19  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac:
        * m4/podsleuth.m4: Only use expanded_libdir for the configure warning;
        use the unexpanded libdir for actual building/installation, which is
        required when in build environments for packaging
    
        * src/PodSleuth/Makefile.am:
        * src/PodSleuth.Hal/Makefile.am: Install PodSleuth specific things always
        into {prefix}/lib and not {libdir} since PodSleuth is noarch technically
    
    
    svn path=/trunk/; revision=52

commit 8e3525d5e062eba2369e9c0de8c53d8b29381c99
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue Dec 18 14:20:33 2007

    Released 0.6.0
    
    2007-12-18  Aaron Bockover  <abock@gnome.org>
    
        Released 0.6.0
    
        * NEWS: Updated for 0.6.0 release
    
    
    svn path=/trunk/; revision=50

commit fa7404fd89037ec0f428814b8ab1f71f37631c15
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue Dec 18 14:11:36 2007

    Fixed color
    
    svn path=/trunk/; revision=49

commit 07ae26772639f55ccc1a5d06cdea99fb00011289
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Fri Oct 12 13:37:18 2007

    Fixed warning text
    
    svn path=/trunk/; revision=48

commit 243291d634d58ec46fe9bb526fa3de044b4f19a0
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Fri Oct 12 13:26:33 2007

    Moved hal callouts dir check to podsleuth.m4
    
    svn path=/trunk/; revision=47

commit e3f6f4139706b2d73a2662c0c53a5f25d773ea03
Author: Aaron Bockover <abock@gnome.org>
Date:   Fri Oct 12 13:20:40 2007

    Reverted last change, libexecdir doesn't seem to be correct; it's only
    
    2007-10-11  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/Makefile.am: Reverted last change, libexecdir doesn't
        seem to be correct; it's only used on Gentoo AFAIK. Now install the
        script into HALCALLOUTSDIR, set by configure
    
        * configure.ac: Added --with-hal-callouts-dir option to configure which
        can be set by users on distributions that completely screw up how they
        install HAL (Gentoo). The default is still libdir.
    
    
    svn path=/trunk/; revision=46

commit c6a6892af752c113bfa9dcb916ce8f1348849d90
Author: Aaron Bockover <abock@gnome.org>
Date:   Fri Oct 12 12:52:37 2007

    Install the HAL script in libexecdir
    
    2007-10-11  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/Makefile.am: Install the HAL script in libexecdir
    
    
    svn path=/trunk/; revision=45

commit 9c28217f604c073a525ee6b96e998cc15233a0db
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Oct 10 12:25:15 2007

    Change org.banshee-project.podsleuth prefix to org.podsleuth so it's
    
    2007-10-10  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs:
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalEntry.cs:
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs:
        * data/20-podsleuth.fdi: Change org.banshee-project.podsleuth prefix
        to org.podsleuth so it's shorter and less annoying
    
    
    svn path=/trunk/; revision=44

commit 03b455c3b50ba181c957605feea4d30aeae6c992
Author: Aaron Bockover <abock@gnome.org>
Date:   Fri Oct 5 15:15:51 2007

    Generate podsleuth.pc
    
    2007-09-17  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac: Generate podsleuth.pc
    
        * data/podsleuth.pc.in: Install podsleuth.pc for build/version checking
    
        * data/ipod-model-table: Added a code for an old iPod
    
    
    svn path=/trunk/; revision=43

commit 604dd2a6def4ad91f52939dc557cc681b044d778
Author: Aaron Bockover <abock@gnome.org>
Date:   Mon Sep 17 09:44:31 2007

    Fix version
    
    2007-09-17  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac: Fix version
    
        * src/PodSleuth/PodSleuth/Device.cs: Read FireWireGUID from the plist
        as it is necessary to create the new iTunesDB hash
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Merge the
        FireWireGUID property into the HAL tree as ipod.firewire_id; dump the
        plist on error if it is available for better debugging
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Print the
        FireWireGUID in the console output
    
        * src/PodSleuth/PropertyList/PlistDocument.cs:
        * src/PodSleuth/PropertyList/PlistData.cs: Added binary <data> object
        support to plist document parser
    
    
    svn path=/trunk/; revision=42

commit 1c5757c7d0e04e5549413f80dc2cbe012d65188b
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Wed Sep 12 13:20:28 2007

    Added PlistDump tool
    
    svn path=/trunk/; revision=41

commit d0e1419eb86d63462d8d646884d29da2251b6263
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Wed Sep 12 13:00:39 2007

    Fixed distcheck
    
    svn path=/trunk/; revision=40

commit 7c783fef0edfb1fefe7ec943c9ff0aa8256114d1
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Wed Sep 12 12:55:02 2007

    Fixed automake problems
    
    svn path=/trunk/; revision=39

commit fd51e5cd06e34d4543eef6e7f26871671a9903d7
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Wed Sep 12 12:29:14 2007

    Link to the model info page at apple.com
    
    svn path=/trunk/; revision=38

commit bc89552718e48dd2ebef97577bbb96eaa719b30c
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Sep 12 12:14:45 2007

    Change version to -pre1 for a test release
    
    2007-09-12  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac: Change version to -pre1 for a test release
    
        * src/PodSleuth/PodSleuth.ModelData/ModelColor.cs: Added Purple
    
        * src/PodSleuth/PodSleuth.ModelData/ModelAttributes.cs: Removed WiFi
        and MultiTouch since those attributes are standard across the model class
    
        * src/PodSleuth/PodSleuth.ModelData/ModelAttributes.cs: Added Phone
    
        * data/ipod-model-table: Updated to include at least one serial code for
        each new shuffle, nano, classic, and phone on display at the Cambridge, MA
        Apple store today
    
    
    svn path=/trunk/; revision=37

commit 6cf6266a91793eb8bf428bf30b102cb391e51e58
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Sep 12 09:17:36 2007

    Much better workaround for the @prefix@ bug in the NDesk DBus .pc file
    
    2007-09-12  Aaron Bockover  <abock@gnome.org>
    
        * m4/podsleuth.m4: Much better workaround for the @prefix@ bug in the
        NDesk DBus .pc file
    
    
    svn path=/trunk/; revision=36

commit bf185eadaa512a9debd211b1f145ba9e3aaa11dd
Author: Aaron Bockover <abock@gnome.org>
Date:   Wed Sep 12 09:08:48 2007

    Stubbed out new iPods (Classic, Touch, Nano 3)
    
    2007-09-12  Aaron Bockover  <abock@gnome.org>
    
        * data/ipod-model-table: Stubbed out new iPods (Classic, Touch, Nano 3)
    
        * m4/podsleuth.m4:
        * src/PodSleuth.Hal/Makefile.am: Link against the installed NDesk DBus
    
        * src/PodSleuth/PodSleuth.ModelData/ModelClass.cs:
        * src/PodSleuth/PodSleuth.ModelData/ModelAttributes.cs: Added values to
        enums to support the Touch and Classic iPods announced recently
    
        * src/NDesk.DBus: Removed bundled sources
    
        * MAINTAINERS: Added
    
    
    svn path=/trunk/; revision=35

commit 609a803714062cb6e3b0c64940adfa11b0b4bfa7
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Fri May 4 17:01:30 2007

    Added TDS model
    
    svn path=/trunk/; revision=34

commit 889e3926e4933cf104765fff6f396c644626da9f
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Thu May 3 21:31:05 2007

    Added more helpful admin targets
    
    svn path=/trunk/; revision=33

commit 939f380429e0ddcf6a1d55c3cac223f72b3d70ab
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 21:25:28 2007

    Added edit-table target
    
    2007-05-04  Aaron Bockover  <abock@gnome.org>
    
        * Makefile.am:
        * data/Makefile.am: Added edit-table target
    
        * data/ipod-model-table: Added new model contributions from the wiki table
    
    
    svn path=/trunk/; revision=32

commit 68e4c47202d066af4f202f8a1213f9192a5b9e60
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 16:25:18 2007

    Update --help
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Update --help
    
    
    svn path=/trunk/; revision=31

commit d157f405be3a58193bed3daf5466187d7b47ac55
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 16:04:05 2007

    Check for ShadowDB in the plist, which indicates the device expects
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth/PodSleuth/Device.cs: Check for ShadowDB in the plist,
        which indicates the device expects iPod_Control/iTunes/iTunesSD to
        be written, indicating we have a shuffle (VERY AWESOME)
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Always print
        device_class since it will now always be set; explain about unknown devices
        if the device is unknown, tell them how to update and rescan, and to
        submit info on the wiki
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Always
        set device_class, even if is_unknown, to either 'shuffle' if we
        have ShadowDB or 'unknown' if not; when called from Scan(), the volume
        will already be mounted, so use the mount from HAL instead of doing
        a private libc mount, which will fail
    
    
    svn path=/trunk/; revision=30

commit dcb1d7b04f58c0a73d1a436a268ab2884a7cc3f0
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 15:19:58 2007

    Set InstallPaths.CacheDirectory at build time
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        * src/AssemblyInfo.cs.in: Set InstallPaths.CacheDirectory at build time
    
        * src/PodSleuth/PodSleuth.ModelData/ModelTable.cs: Added DownloadTableUpdate
        method to download and save an update model table
    
        * src/PodSleuth/PodSleuth/Device.cs: If an updated table exists, load
        it instead of the embedded table resource
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Support
        Scan and UpdateModelTable methods via command line arguments; do not
        try to read model properties if is_unknown; fix read only detection
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalEntry.cs: If --update is
        passed, download an updated table
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Clear any
        strlist properties before appending so subsequent Scan() calls don't
        result in stale data; merge capabilities before is_unknown check as
        they don't pertain to model information necessarily
    
        * src/PodSleuth.Hal/hal-podsleuth.in: Support running in --update mode
    
        * data/20-podsleuth.fdi: Added a org.banshee_project.podsleuth interface
        and two methods on it: Scan and UpdateModelTable; applications can use
        these methods in conjunction with checking is_unknown to download the
        latest model detection table and refresh the device
    
    
    svn path=/trunk/; revision=29

commit 3012dc952f5b37e6e55eb4e1b3a9f33f5b6831c5
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 11:44:59 2007

    Generate src/AssemblyInfo.cs
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac: Generate src/AssemblyInfo.cs
    
        * src/AssemblyInfo.cs.in: Assembly information
    
        * src/Makefile.am: EXTRA_DIST AssemblyInfo.cs
    
        * src/PodSleuth/Makefile.am:
        * src/PodSleuth.Hal/Makefile.am: Include src/AssemblyInfo.cs in build
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Read/format
        version information from assembly
    
    
    svn path=/trunk/; revision=28

commit 907c62a9d59a877ab5d233f991bcd9bf92f25e83
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 10:41:02 2007

    Class to read and represent image format information from plist format
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth/PodSleuth/ImageFormat.cs: Class to read and represent
        image format information from plist format
    
        * src/PodSleuth/PodSleuth/Device.cs: Load image format data from plist
    
        * src/PodSleuth/PodSleuth/ImageType.cs: Enum of image types supported
    
        * src/PodSleuth/PodSleuth/PixelFormat.cs: Enum of pixel formats supported
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Load image
        format information into HAL; revised some property names
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Updated to
        show image format information and use updated property names
    
        * tests/ImageTest.cs:
        * tests/Makefile.am: Added a test for image information parsing
    
    
    svn path=/trunk/; revision=27

commit e57343c53f47fdf2e47fc97dc6599cf6bf91e11e
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Thu May 3 07:54:24 2007

    Updated
    
    svn path=/trunk/; revision=26

commit 99f4e800cb76eb3a2ce963ddb0135f2e4e274e65
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Thu May 3 07:53:36 2007

    Updated
    
    svn path=/trunk/; revision=25

commit 14a5fab7c01599424b349ffbf47ffe105cdd5f5c
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Thu May 3 07:52:28 2007

    Do not intltoolize
    
    svn path=/trunk/; revision=24

commit ab4b5f0337a32e5e368087a30dc4b321bbbdde26
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 07:51:46 2007

    Removed i18n support as it's not going to be necessary
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        Removed i18n support as it's not going to be necessary
    
    
    svn path=/trunk/; revision=23

commit 837835848a39e242b825148e0ae9fed42bb15659
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu May 3 07:49:56 2007

    Reverted all major changes regarding updating and notifications, it's a
    
    2007-05-03  Aaron Bockover  <abock@gnome.org>
    
        Reverted all major changes regarding updating and notifications, it's
        a bad idea and should be handled in applications
    
    
    svn path=/trunk/; revision=22

commit 88dcf6e86f1dabf03083e62a7d04139e31b02c5a
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue May 1 14:11:33 2007

    Build properly
    
    svn path=/trunk/; revision=21

commit 301a460fa5163488bbbf3c3a36f4f694847e14b5
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue May 1 14:07:15 2007

    Install the .config file
    
    svn path=/trunk/; revision=20

commit d04dc327cd320e57abecb380b18d3fc8c6c6e684
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue May 1 14:06:06 2007

    Generate src/PodSleuth.Hal/PodSleuth.Hal.exe.config.in
    
    2007-05-01  Aaron Bockover  <abock@gnome.org>
    
        * configure.ac: Generate src/PodSleuth.Hal/PodSleuth.Hal.exe.config.in
    
        * src/PodSleuth.Hal/Notifications/Notification.cs: Update only needs
        GLib access, a requirement
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/UnknownPodHandler.cs: Show
        a notification asking if PodSleuth should check for a table update
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Temporarily
        run the unknown handler (for testing)
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalEntry.cs: Moved the testing
        into the UnknownPodHandler
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Run the
        UnknownPodHandler if the iPod is unknown
    
        * src/NDesk.DBus.GLib: Added GLib/DBus integration
    
    
    svn path=/trunk/; revision=19

commit 13a63c7c99bf0622859f73cc14527c42a1ce2ac7
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue May 1 12:47:58 2007

    Added a test to read the Plist from SCSI
    
    2007-05-01  Aaron Bockover  <abock@gnome.org>
    
        * tests/PlistTest.cs:
        * tests/Makefile.am: Added a test to read the Plist from SCSI
    
    
    svn path=/trunk/; revision=18

commit 0748335f5260e38e4ca09a0fb2e01a597e6c7f5d
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue May 1 11:34:39 2007

    Added/updated to support i18n, it will be needed soon
    
    2007-05-01  Aaron Bockover  <abock@gnome.org>
    
        * autogen.sh:
        * configure.ac:
        * Makefile.am:
        * po/:
        * m4/i18n.m4: Added/updated to support i18n, it will be needed soon
    
        * src/PodSleuth.Hal/Notifications/Notification.cs:
        * src/PodSleuth.Hal/Notifications/Notifications.cs: Added notification
        support APIs
    
    
    svn path=/trunk/; revision=16

commit 80a478f88469c1ed4037b0b07a19ba16ffd57685
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue May 1 10:27:00 2007

    Updated
    
    svn path=/trunk/; revision=15

commit 0c3bb8883429a3b237d78d57503f946cb4f6c946
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue May 1 09:53:09 2007

    Nuked the NetLoader stuff, it's a dumb idea
    
    2007-05-01  Aaron Bockover  <abock@gnome.org>
    
        Nuked the NetLoader stuff, it's a dumb idea
    
    
    svn path=/trunk/; revision=14

commit b13e2f81d0dfb4b4edbd58cba9588c79b7037ad8
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue May 1 09:32:29 2007

    Renamed NetLoader to BootLoader
    
    svn path=/trunk/; revision=13

commit 916a164014b106f274dfb162fd69dd547b45c5f4
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue May 1 09:31:57 2007

    Renamed NetLoader to BootLoader
    
    svn path=/trunk/; revision=12

commit 78754feb35688d6ceb85e5028e4586fce0d4c0d6
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue May 1 09:31:23 2007

    Updated
    
    svn path=/trunk/; revision=11

commit 81b2831b4020876c6543158d48fafc0acebee059
Author: Aaron Bockover <abock@gnome.org>
Date:   Mon Apr 30 20:27:03 2007

    Load the assembly explicitly from args[0] and use the updates dir at
    
    2007-04-30  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.NetLoader/PodSleuth.NetLoader/NetLoaderEntry.cs: Load
        the assembly explicitly from args[0] and use the updates dir at args[1];
        pass args[2]..args[N] to the real runtime assembly
    
        * src/PodSleuth.Hal/podsleuth.in:
        * src/PodSleuth.Hal/hal-podsleuth.in: Run the NetLoader which will
        bootstrap the main assembly at runtime, after performing optional
        internet updates
    
        * configure.ac: Run PODSLEUTH_CHECK_UPDATE_DIR, print a summary
    
        * m4/podsleuth.m4: Added PODSLEUTH_CHECK_UPDATE_DIR
    
        * Makefile.am: Remove libdir and update dir during uninstall if empty
    
    
    svn path=/trunk/; revision=10

commit 75d767dd798c314e8d0e1c35841322a66933b490
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Mon Apr 30 19:24:06 2007

    Updated
    
    svn path=/trunk/; revision=9

commit 3115d06d8d1a6fc83c57b61c8b3d1add962be654
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Mon Apr 30 19:16:58 2007

    Symlink
    
    svn path=/trunk/; revision=8

commit a90de6def7a28a5d8efebbe8489477fbf03fdb01
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Mon Apr 30 19:16:27 2007

    Symlink
    
    svn path=/trunk/; revision=7

commit f958798c4ced654df24ff16291d3d9a80e5c6b8b
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Mon Apr 30 19:15:29 2007

    Added initial NetLoader
    
    svn path=/trunk/; revision=6

commit 8ad08e62b95d306be76af265a53711ebaf7e8ecb
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Mon Apr 30 19:14:57 2007

    Moved out NDesk.DBus
    
    svn path=/trunk/; revision=5

commit 14587a0d9649e3360626c4fa5a3830771c1941e9
Author: Aaron Bockover <abock@gnome.org>
Date:   Thu Apr 19 09:37:07 2007

    Added XQX for 2nd gen blue shuffle, 1GB
    
    2007-04-19  Aaron Bockover  <abock@gnome.org>
    
        * data/ipod-model-table: Added XQX for 2nd gen blue shuffle, 1GB
    
    
    svn path=/trunk/; revision=4

commit 4732d2583824be5db4afece059700979ed71c025
Author: Aaron Bockover <abock@gnome.org>
Date:   Tue Apr 3 22:11:20 2007

    Write exception log to a debug file if there's a failure so the podsleuth
    
    2007-04-04  Aaron Bockover  <abock@gnome.org>
    
        * src/PodSleuth.Hal/hal-podsleuth.in: Write exception log to a debug
        file if there's a failure so the podsleuth client can warn and provide
        more information
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalPopulator.cs: Merge more
        properties into the device
    
        * src/PodSleuth.Hal/PodSleuth.HalFrontend/HalClient.cs: Read more
        properties now supported; warn if there's a debug log present when a
        device cannot be sleuthed
    
        * src/PodSleuth.Hal/Hal/Device.cs: Added property list methods
    
        * src/PodSleuth/PodSleuth/DeviceCapabilities.cs: Added capabilities an
        iPod may have above and beyond its music playing duties (vcard, ical,
        podcast...)
    
        * src/PodSleuth/PodSleuth/DevicePaths.cs: Do not crash if directories
        don't exist
    
        * src/PodSleuth/PodSleuth/Device.cs: Exposed device paths, the plist
        document, capabilities, and firmware version; read firmware version and
        capabilities from the plist
    
        * src/PodSleuth/PodSleuth.ModelData/ModelAttributes.cs: Fixed values
    
    
    svn path=/trunk/; revision=3

commit 384ef59fdcd41bf32ba6d99b342fc77fc6e88e89
Author: Aaron Bockover <abock@src.gnome.org>
Date:   Tue Apr 3 10:22:26 2007

    Initial import of PodSleuth
    
    svn path=/trunk/; revision=2
